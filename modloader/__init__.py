import os
import importlib.util
import inspect

MODS_LOCATION = './mods/'

class BaseMod:
    def initialization(self, generator, story_manager):
        pass

    def game_start(self, generator, story_manager):
        pass
    
    def try_handle_command(self, command, args, generator, story_manager):
        return False
    
    def get_input(self):
        pass

    def on_input(self, action, generator, story_manager):
        return action

    def preprocess_action(self, action):
        return (action, True)
    
    def instructions(self):
        return []

class ModLoader:
    def __init__(self):
        super().__init__()
        self.mods = []

    def initialization(self, generator, story_manager):
        for (mod, enabled) in self.mods:
            if enabled:
                mod.initialization(generator, story_manager)
    
    def game_start(self, generator, story_manager):
        for (mod, enabled) in self.mods:
            if enabled:
                mod.game_start(generator, story_manager)
    
    def try_handle_command(self, command, args, generator, story_manager):
        for (mod, enabled) in self.mods:
            if enabled:
                if mod.try_handle_command(command, args, generator, story_manager):
                    return True
        return False

    def get_input(self):
        for (mod, enabled) in self.mods:
            if enabled:
                res = mod.get_input()
                if res is not None:
                    return res
        return input("> ").strip()
    
    def on_input(self, action, generator, story_manager):
        for (mod, enabled) in self.mods:
            if enabled:
                action = mod.on_input(action, generator, story_manager)
        return action
    
    def preprocess_action(self, action):
        continue_processing = True
        for (mod, enabled) in self.mods:
            if enabled:
                (action, allows_processing) = mod.preprocess_action(action)
                continue_processing = continue_processing and allows_processing
        return (action, continue_processing)

    def instructions(self):
        ins = []
        for (mod, enabled) in self.mods:
            if enabled:
                ins += mod.instructions()
        return ins

    def load_mods(self):
        try:
            for filename in os.listdir(MODS_LOCATION):
                if filename == '__pycache__':
                    continue
                try:
                    spec = importlib.util.spec_from_file_location(f'mod_{filename}', os.path.join(MODS_LOCATION, filename, 'mod.py'))
                    mod = importlib.util.module_from_spec(spec)
                    spec.loader.exec_module(mod)
                    for (name, mod_class) in inspect.getmembers(mod):
                        if mod_class is not BaseMod and type(mod_class) == type and issubclass(mod_class, BaseMod):
                            self.mods.append((mod_class(), True))
                            print(f'Loaded class {name} from mod {filename}')
                except Exception as e:
                    print(f'Warning: could not load mod {filename}, exception: {e}')
        except:
            print('Warning: Could not find mods folder')
    
    def configure_mods(self):
        message = None
        while True:
            # Print mods
            print('Mod configuration')
            print(' # | Enabled | Mod Name')
            print('-----------------------')
            for (i, (mod, enabled)) in enumerate(self.mods):
                print('%2s | [%s]     | %s' % (i, 'x' if enabled else ' ', type(mod).__name__))
            print('Config commands:')
            print('exit')
            print('enable #')
            print('disable #')
            if message:
                print(message)
            response = input('> ')
            args = response.split(' ')
            command = args[0].lower()
            if command == 'exit':
                break
            elif command == 'enable':
                try:
                    idx = int(args[1])
                    self.mods[idx] = (self.mods[idx][0], True)
                    message = f'Enabled "{type(self.mods[idx][0]).__name__}"'
                except Exception as e:
                    message = f'Exception while trying to enable mod {args[1]}: {e}'
            elif command == 'disable':
                try:
                    idx = int(args[1])
                    self.mods[idx] = (self.mods[idx][0], False)
                    message = f'Disabled "{type(self.mods[idx][0]).__name__}"'
                except Exception as e:
                    message = f'Exception while trying to disable mod {args[1]}: {e}'
            else:
                message = f'Unknown command "{command}"'
